package ru.kalichkin.dto;

import ru.kalichkin.model.Contact;
import ru.kalichkin.model.ContactValidation;


public class ContactDto {
    private ContactValidation contactValidation;
    private Contact contact;

    public ContactValidation getContactValidation() {
        return contactValidation;
    }

    public void setContactValidation(ContactValidation contactValidation) {
        this.contactValidation = contactValidation;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
}

