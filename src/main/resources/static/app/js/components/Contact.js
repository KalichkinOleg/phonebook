export default function Contact(id, lastName, firstName, phone, important, checked) {
    this.id = id;
    this.lastName = lastName;
    this.firstName = firstName;
    this.phone = phone;
    this.important = important;
    this.checked = checked;
}
