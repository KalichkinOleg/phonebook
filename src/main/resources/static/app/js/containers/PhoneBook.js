import React, {Component} from 'react';
import Lazy from 'lazy.js/lazy';
import '../../css/phonebook.css';
import {connect} from 'react-redux';
import InputForm from '../components/InputForm';
import * as action from "../action/actionContacts";
import {Button, ButtonToolbar, Col, Grid, Modal, Row} from "react-bootstrap";
import CallTable from "../components/CallTable";
import * as actionCall from "../action/actionCalls";
import ContactTable from "../components/ContactTable";
import * as actionChecked from "../action/actionChecked";


class PhoneBook extends Component {
    constructor(props) {
        super(props);
        this.state = {
            callsTableVisible: false,
            showModal: false,
            title: '',
            body: '',
            removeSelected: false,
            markSelected: false,
        };
    };

    componentWillMount() {
        this.props.onLoadContactsFromServer();
        this.props.onLoadCallsFromServer();
    }


    render() {
        let isChecked = !Lazy(this.props.checked).contains(true);
        return (
            <Grid bsClass="content">
                <Modal bsSize="sm" backdrop={'static'} show={this.state.showModal} onHide={() => {
                    this.closeDialog()
                }}>
                    <Modal.Header closeButton>
                        <Modal.Title><strong>{this.state.title}</strong></Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{this.state.body}</Modal.Body>
                    <Modal.Footer>
                        <Button bsClass="btn" bsStyle={isChecked ? 'primary' : 'default'} onClick={() => {
                            this.closeDialog()
                        }}>{isChecked ? 'Ок' : 'Отмена'}</Button>
                        <Button bsClass={isChecked ? 'hidden' : 'btn'} bsStyle="primary"
                                onClick={() => {
                                    this.clickOkDialog()
                                }}>Да</Button>
                    </Modal.Footer>
                </Modal>
                <div className='filter-container'>
                    <input type='text' placeholder='Поиск...' className='form-control input-sm form-input'
                           onChange={(e) => {
                               this.props.onFindContact(e)
                           }}/>
                </div>
                <Row>
                    <Col md={6}>
                        <ContactTable
                            contacts={this.props.contacts}
                            onSelectAllCheckbox={(event) => {
                                this.props.onSelectAllCheckbox(event)
                            }}
                            onRemoveContact={(id) => {
                                this.props.onRemoveContact(id)
                            }} checked={this.props.checked}
                            onChangeChecked={(index, event) => {
                                this.props.onChangeChecked(index, event)
                            }}
                            onAddCheckbox={(checked) => {
                                this.props.onAddCheckbox(checked)
                            }}
                            onAddCall={(contact) => {
                                this.props.onAddCall(contact)
                            }}
                            callsTableVisible={this.state.callsTableVisible}
                        />

                        <ButtonToolbar>
                            <Button type='button' bsClass='btn' bsSize="sm" bsStyle="primary" onClick={() => {
                                this.removeSelectedContacts()
                            }}>Удалить выбранные
                            </Button>

                            <Button type='button' bsClass='btn' bsSize="sm" bsStyle="primary" onClick={() => {
                                this.markSelectedContacts()
                            }}>Установить/снять приоритет
                            </Button>

                            <Button type='button' bsClass='btn' bsSize="sm" bsStyle="info" onClick={() => {
                                this.visibleCallsTable()
                            }}> Показать/скрыть звонки
                            </Button>
                        </ButtonToolbar>

                        <div className='form'>
                            <form className='form-field' method='get'
                                  action='/phoneBook/rcp/api/v1/contact/download'>
                                <Button type='submit' bsClass='btn' bsSize="sm" bsStyle="success">Выгрузить контакты
                                    в Excel
                                </Button>
                            </form>
                        </div>

                        <br/>

                        <InputForm contacts={this.props.contacts}
                                   onAddContact={(lastName, firstName, phone) => {
                                       this.props.onAddContact(lastName, firstName, phone)
                                   }}/>
                    </Col>
                    <Col md={6}>
                        <CallTable
                            calls={this.props.calls} onRemoveCall={(id) => {
                            this.props.onRemoveCall(id)
                        }}
                            className={this.state.callsTableVisible ? 'table table-striped  call-table table-hover' : 'hidden'}
                        />
                    </Col>

                </Row>
            </Grid>
        )
    }

    visibleCallsTable() {
        this.setState({
            callsTableVisible: !this.state.callsTableVisible
        })
    }


    closeDialog() {
        this.setState({
            showModal: false,
            removeSelected: false,
            markSelected: false
        });
    }


    removeSelectedContacts() {
        if (!Lazy(this.props.checked).contains(true)) {
            this.setState({
                showModal: true,
                title: 'Удаление контактов',
                body: 'Нет выбранных контактов!'
            });
        } else {
            this.setState({
                showModal: true,
                title: 'Удаление контактов',
                body: 'Вы уверены, что хотите удалить выбранные контакты?',
                removeSelected: true
            });
        }
    }

    markSelectedContacts() {
        if (!Lazy(this.props.checked).contains(true)) {
            this.setState({
                showModal: true,
                title: 'Приоритет',
                body: 'Нет выбранных контактов!'
            });
        } else {
            this.setState({
                showModal: true,
                title: 'Приоритет',
                body: 'Вы уверены, что хотите установить/снять приоритет выбранных контактов?',
                markSelected: true
            });
        }
    }

    clickOkDialog() {
        if (this.state.removeSelected) {
            this.props.onRemoveSelectedContacts(this.props.contacts);
            this.setState({
                removeSelected: false
            })
        } else if (this.state.markSelected) {
            this.props.onMarkSelectedContacts(this.props.contacts);
            this.setState({
                markSelected: false
            })
        }

        this.setState({
            showModal: false
        })
    }
}


export default connect(
    state => ({
        contacts: Lazy(state.contacts.items).filter(contact => {
            let searchValue = contact.lastName.toLowerCase() + contact.firstName.toLowerCase() + contact.phone.toLowerCase();
            return searchValue.indexOf(state.filterContact) !== -1;
        }).toArray(),
        calls: state.calls.items,
        checked: state.checked
    }),
    dispatch => ({
        onLoadContactsFromServer: () => {
            dispatch(action.getAllContactsFromServer())
        },
        onFindContact: (event) => {
            dispatch(action.findContact(event))
        },
        onAddContact: (lastName, firstName, phone) => {
            dispatch(action.addContact(lastName, firstName, phone));
        },
        onRemoveContact: (id) => {
            dispatch(action.removeContact(id))
        },
        onRemoveSelectedContacts: (contacts) => {
            dispatch(action.removeSelectedContacts(contacts));
        },
        onMarkSelectedContacts: (contacts) => {
            dispatch(action.markSelectedContacts(contacts));
        },
        onLoadCallsFromServer: () => {
            dispatch(actionCall.getAllCallsFromServer())
        },
        onAddCall: (contact) => {
            dispatch(actionCall.addCall(contact));
        },
        onRemoveCall: (id) => {
            dispatch(actionCall.removeCall(id))
        },
        onAddCheckbox: (checked) => {
            dispatch(actionChecked.addCheckbox(checked))
        },
        onChangeChecked: (index, event) => {
            dispatch(actionChecked.changeChecked(index, event))
        },
        onSelectAllCheckbox: (event) => {
            dispatch(actionChecked.selectAllCheckbox(event))
        }
    })
)(PhoneBook)


