import * as types from "../constants/actionTypes";

export default function filterContact(state = '', action) {
    switch (action.type) {
        case types.FIND_CONTACT:
            return action.payload;
            break;
        default:
            return state;
    }
}
