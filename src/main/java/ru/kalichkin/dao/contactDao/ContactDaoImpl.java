package ru.kalichkin.dao.contactDao;

import org.springframework.stereotype.Repository;
import ru.kalichkin.dao.GenericDaoImpl;
import ru.kalichkin.model.Contact;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ContactDaoImpl extends GenericDaoImpl<Contact, Long> implements ContactDao {


    public ContactDaoImpl() {
        super(Contact.class);
    }


    @Override
    public List<Contact> getAllContacts() {
        return findAll();
    }


    @Override
    public void save(Contact contact) {
        saveOrUpdate(contact);
    }


    @Override
    public List<Contact> findByPhone(String phone) {
        Map<String, Object> condition = new HashMap();
        condition.put("phone", phone);
        return findAllByMulti(condition);
    }


    @Override
    public void removeContact(int contactId) {
        remove(contactId);
    }


    @Override
    public void removeSelected(int[] contactsId) {
        List<Contact> contacts = getAllContacts();
        for (Contact contact : contacts) {
            for (int id : contactsId) {
                if (contact.getId() == id && !contact.isImportant()) {
                    remove(id);
                    break;
                }
            }
        }
    }


    @Override
    public void markSelected(int[] contactsId) {
        List<Contact> contacts = getAllContacts();
        boolean isMarked;
        for (Contact contact : contacts) {
            isMarked = false;
            for (int id : contactsId) {
                if (isMarked) {
                    continue;
                }
                if (contact.getId() == id) {
                    if (!contact.isImportant()) {
                        contact.setImportant(true);
                        isMarked = true;
                    } else {
                        contact.setImportant(false);
                        isMarked = true;
                    }
                    saveOrUpdate(contact);
                }
            }
        }
    }
}
