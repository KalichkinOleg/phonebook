# README #
Это проект - телефонная книга, с добавлением совершенных звонков, реализованная на следующих технологиях:

Клиентская часть: HTML, CSS, JS, React, Redux, Webpack, React-bootstrap, Bootstrap, Lazy.js, babel. Серверная часть: Spring-framework. Связь БД и Java: Hibernate. БД: MySQL установленная на компьютероре, схема "phonebook", логин и пароль "root". Сборка: Spring + Maven. Запуск: Spring + Tomcat.

Что умеет делать проет: отображать сущетсвующие контакты, добавлять новые контакты, удалять контакты, отмечать контакты, как важные, имитировать добавление звонка.

Как запустить проект: 1.Из папки static в консоли вызвать команду npm install и npm run build. 2.Запустить Run PhoneBookApplication - данная команда собирает проект и запускает PhoneBookSpringApplication main.

Реализованные методы API: GET {host}/phoneBook/rcp/api/v1/contact/getAll - получениe списка контактов.

POST {host}//phoneBook/rcp/api/v1/contact/add - добавление нового контакта в формате: Тело запроса: { "firstName": "Имя", "lastName": "Фамилия", "phone": "Телефон" }

POST {host}//phoneBook/rcp/api/v1/contact/remove - удаление контакта. Тело запроса: номер идентификатора

POST {host}//phoneBook/rcp/api/v1/contact/removeSelected - удаление выбранных контактов. Тело запроса: [...id]

POST {host}//phoneBook/rcp/api/v1/contact/markSelected - установка/снятие приоритета выбранных контактов. Тело запроса: [...id]

GET {host}/phoneBook/rcp/api/v1/call/getAll - получения списка звонков.

POST {host}//phoneBook/rcp/api/v1/call/remove - удаление звонка Тело запроса: номер идентификатора