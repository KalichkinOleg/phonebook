import React, {Component} from 'react';
import Lazy from 'lazy.js/lazy';
import TableRow from "./TableRow";
import Checkbox from "./Checkbox";

export default class ContactTable extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let isAllChecked = this.props.checked.length === 0 ? false
            : Lazy(this.props.checked).filter(c => c).toArray().length
            === this.props.checked.length;

        return (
            <table className="table table-striped contact-table table-hover">
                <thead>
                <tr className='text-center'>
                    <th className='text-center'>
                        <label>
                            <Checkbox onChange={(event) => {
                                this.props.onSelectAllCheckbox(event)
                            }} checked={isAllChecked}/>
                        </label>
                    </th>
                    <th className='text-center'>№</th>
                    <th className='text-center'>Фамилия</th>
                    <th className='text-center'>Имя</th>
                    <th className='text-center'>Телефон</th>
                    <th className='text-center' colSpan={2}>Действия</th>
                </tr>
                </thead>
                <tbody>
                {
                    Lazy(this.props.contacts).map((contact, i) =>
                        <TableRow key={contact.id}
                                  number={i + 1}
                                  contact={contact}
                                  onRemoveContact={(id) => {
                                      this.props.onRemoveContact(id);
                                  }}
                                  checked={this.props.checked[i]}
                                  onChangeChecked={(event) => {
                                      this.props.onChangeChecked(i, event)
                                  }}
                                  onAddCheckbox={() => {
                                      this.props.onAddCheckbox(false)
                                  }}
                                  onAddCall={(contact) => {
                                      this.props.onAddCall(contact)
                                  }}
                                  callsTableVisible={this.props.callsTableVisible}
                        />).toArray()
                }
                </tbody>
            </table>
        )
    }
}

