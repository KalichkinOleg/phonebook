package ru.kalichkin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kalichkin.dao.contactDao.ContactDao;
import ru.kalichkin.model.Contact;
import ru.kalichkin.model.ContactValidation;

import java.util.List;

@Service
public class ContactService {

    @Autowired
    private ContactDao contactDao;

    private boolean isExistContactWithPhone(String phone) {
        List<Contact> contactList = contactDao.findByPhone(phone);
        return !contactList.isEmpty();
    }

    private ContactValidation setErrorMessage(ContactValidation contactValidation, String message) {
        contactValidation.setValid(false);
        contactValidation.setError(message);
        return contactValidation;
    }

    private ContactValidation validateContact(Contact contact) {
        ContactValidation contactValidation = new ContactValidation();
        contactValidation.setValid(true);

        if (contact.getFirstName().isEmpty()) {
            return setErrorMessage(contactValidation, "Поле Имя должно быть заполнено.");
        }

        if (contact.getLastName().isEmpty()) {
            return setErrorMessage(contactValidation, "Поле Фамилия должно быть заполнено.");
        }

        if (contact.getPhone().isEmpty()) {
            return setErrorMessage(contactValidation, "Поле Телефон должно быть заполнено.");
        }

        if (isExistContactWithPhone(contact.getPhone())) {
            return setErrorMessage(contactValidation, "Номер телефона не должен дублировать другие номера в телефонной книге.");
        }

        return contactValidation;
    }

    public ContactValidation addContact(Contact contact) {
        ContactValidation contactValidation = validateContact(contact);
        if (contactValidation.isValid()) {
            contactDao.save(contact);
        }
        return contactValidation;
    }

    public List<Contact> getAllContacts() {
        return contactDao.getAllContacts();
    }

    public void removeContact(int contactId) {
        contactDao.removeContact(contactId);
    }

    public void removeSelectedContact(int[] contactsId) {
        contactDao.removeSelected(contactsId);
    }

    public void markSelectedContact(int[] contactsId) {
        contactDao.markSelected(contactsId);
    }
}
