package ru.kalichkin.dao.callDao;

import org.springframework.stereotype.Repository;
import ru.kalichkin.dao.GenericDaoImpl;
import ru.kalichkin.model.Call;

import java.util.List;

@Repository
public class CallDaoImpl extends GenericDaoImpl<Call, Long> implements CallDao {

    public CallDaoImpl() {
        super(Call.class);
    }

    @Override
    public List<Call> getAllCalls() {
        return findAll();
    }

    @Override
    public Call save(Call call) {
        saveOrUpdate(call);
        return call;
    }

    @Override
    public void removeCall(int callId) {
        remove(callId);
    }
}
