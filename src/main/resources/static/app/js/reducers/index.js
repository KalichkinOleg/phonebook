import {combineReducers} from 'redux'
import contacts from "./contacts";
import calls from "./calls";
import filterContact from "./filterContact";
import checked from "./checked";

export default combineReducers({
    contacts,
    calls,
    filterContact,
    checked
})