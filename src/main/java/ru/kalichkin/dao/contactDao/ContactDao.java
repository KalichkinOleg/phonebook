package ru.kalichkin.dao.contactDao;

import ru.kalichkin.model.Contact;

import java.util.List;

public interface ContactDao {

    List<Contact> getAllContacts();

    void save(Contact contact);

    List findByPhone(String phone);

    void removeContact(int contactId);

    void removeSelected(int[] contactsId);

    void markSelected(int[] contactsId);
}
