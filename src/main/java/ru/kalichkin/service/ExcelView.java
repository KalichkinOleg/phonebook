package ru.kalichkin.service;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.usermodel.*;
import ru.kalichkin.model.Contact;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

public class ExcelView {
    public static void createFile(ContactService contactService, HttpServletResponse response) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet();
        sheet.setDisplayGridlines(false);

        createHeaderTable(workbook, sheet);

        List<Contact> contactList = contactService.getAllContacts();

        int rowNum = 1;
        for (Contact aContactList : contactList) {
            createTable(workbook, sheet, ++rowNum, aContactList);
        }

        response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-Disposition", "attachment; filename=Contacts.xlsx");

        workbook.write(response.getOutputStream());
    }

    private static void createHeaderTable(XSSFWorkbook workbook, XSSFSheet sheet) {
        int beginNumberColumn = 1;
        XSSFRow row = sheet.createRow(beginNumberColumn);
        row.createCell(1).setCellValue("№");
        row.createCell(2).setCellValue("Фамилия");
        row.createCell(3).setCellValue("Имя");
        row.createCell(4).setCellValue("Телефон");

        XSSFFont font = workbook.createFont();
        font.setBold(true);
        XSSFCellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(font);
        cellStyle.setBorderBottom(BorderStyle.MEDIUM);
        cellStyle.setBorderTop(BorderStyle.MEDIUM);
        cellStyle.setBorderLeft(BorderStyle.MEDIUM);
        cellStyle.setBorderRight(BorderStyle.MEDIUM);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setShrinkToFit(true);


        for (int i = beginNumberColumn; i < row.getPhysicalNumberOfCells() + beginNumberColumn; i++) {
            row.getCell(i).setCellStyle(cellStyle);
        }
    }

    private static void createTable(XSSFWorkbook workbook, XSSFSheet sheet, int rowNum, Contact contact) {
        int beginNumberColumn = 1;
        XSSFRow row = sheet.createRow(rowNum);
        row.createCell(1).setCellValue(rowNum - beginNumberColumn);
        row.createCell(2).setCellValue(contact.getLastName());
        row.createCell(3).setCellValue(contact.getFirstName());
        row.createCell(4).setCellValue(contact.getPhone());

        XSSFCellStyle style = workbook.createCellStyle();
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setShrinkToFit(true);
        style.setWrapText(true);


        for (int i = beginNumberColumn; i < row.getPhysicalNumberOfCells() + beginNumberColumn; i++) {
            row.getCell(i).setCellStyle(style);
            sheet.autoSizeColumn(i);
        }
    }
}