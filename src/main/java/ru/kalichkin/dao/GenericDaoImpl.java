package ru.kalichkin.dao;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Transactional
public class GenericDaoImpl<T, PK extends Serializable> implements GenericDao<T, PK> {

    @Autowired
    private SessionFactory sessionFactory;

    private Class<T> clazz;

    public GenericDaoImpl(Class<T> type) {
        this.clazz = type;
    }

    @Transactional
    @Override
    public void saveOrUpdate(T obj) {
        sessionFactory.getCurrentSession().saveOrUpdate(obj);
    }

    @Override
    public T getById(PK id) {
        return (T) sessionFactory.getCurrentSession().get(clazz, id);
    }

    @Transactional
    @SuppressWarnings("unchecked")
    @Override
    public List<T> findAllByMulti(Map<String, Object> condition) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(clazz);
        condition.forEach((k, v) -> {
            if (k != null) {
                criteria.add(Restrictions.eq(k, v));
            }
        });
        return (List<T>) criteria.list();
    }

    @Transactional
    @Override
    public List<T> findAll() {
        return findAll(null);
    }

    @Transactional
    @SuppressWarnings("unchecked")
    @Override
    public List<T> findAll(Order order) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(clazz);
        if (order != null) {
            criteria.addOrder(order);
        }
        return (List<T>) criteria.list();
    }

    @Transactional
    @Override
    public void remove(int id) {
        Object obj = sessionFactory.getCurrentSession().get(clazz, id);
        sessionFactory.getCurrentSession().delete(obj);
    }
}
