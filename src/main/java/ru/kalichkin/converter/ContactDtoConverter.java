package ru.kalichkin.converter;

import org.springframework.stereotype.Component;
import ru.kalichkin.dto.ContactDto;
import ru.kalichkin.model.Contact;
import ru.kalichkin.model.ContactValidation;

@Component
public class ContactDtoConverter {

    public ContactDto map(ContactValidation contactValidation, Contact contact) {
        ContactDto contactDto = new ContactDto();

        Contact contactFromDto = new Contact();
        ContactValidation contactValidationFromDto = new ContactValidation();
        contactFromDto.setId(contact.getId());
        contactFromDto.setFirstName(contact.getFirstName());
        contactFromDto.setLastName(contact.getLastName());
        contactFromDto.setPhone(contact.getPhone());
        contactFromDto.setImportant(contact.isImportant());

        contactValidationFromDto.setValid(contactValidation.isValid());
        contactValidationFromDto.setError(contactValidation.getError());

        contactDto.setContact(contactFromDto);
        contactDto.setContactValidation(contactValidationFromDto);

        return contactDto;
    }
}
