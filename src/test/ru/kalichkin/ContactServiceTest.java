package ru.kalichkin;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kalichkin.model.Contact;
import ru.kalichkin.service.ContactService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ContactServiceTest {

    @Autowired
    private ContactService contactService;

    @Before
    public void initContactService() {
        Contact contact = new Contact();
        contact.setFirstName("Иван");
        contact.setLastName("Иванов");
        contact.setPhone("9123456789");
        contactService.addContact(contact);

        Contact contact2 = new Contact();
        contact2.setFirstName("Петр");
        contact2.setLastName("Петров");
        contact2.setPhone("9012345678");
        contactService.addContact(contact2);
    }


    @Test
    public void testAddContact() {
        int size = contactService.getAllContacts().size();
        Contact testContact = new Contact();
        testContact.setFirstName("Тест");
        testContact.setLastName("Тест");
        testContact.setPhone("0123456789");
        contactService.addContact(testContact);
        assertEquals(size + 1, contactService.getAllContacts().size());
    }

    @Test
    public void testRemoveContact() {
        int size = contactService.getAllContacts().size();
        int contactId = contactService.getAllContacts().get(0).getId();
        contactService.removeContact(contactId);
        assertEquals(size - 1, contactService.getAllContacts().size());
    }

    @Test
    public void testRemoveSelectedContacts() {
        int size = contactService.getAllContacts().size();
        Contact contact1 = contactService.getAllContacts().get(0);
        Contact contact2 = contactService.getAllContacts().get(1);
        int[] contactsId = {contact1.getId(), contact2.getId()};
        contactService.removeSelectedContact(contactsId);
        assertEquals(size - contactsId.length, contactService.getAllContacts().size());
    }


    @Test
    public void testMarkSelectedContacts() {
        Contact testContact = contactService.getAllContacts().get(0);
        int[] contactsId = {testContact.getId()};
        contactService.markSelectedContact(contactsId);
        assertTrue(testContact.isImportant());
    }
}
