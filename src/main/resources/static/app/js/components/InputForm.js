import React, {Component} from 'react';
import Lazy from 'lazy.js/lazy';
import Button from "react-bootstrap/es/Button";

export default class InputForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lastName: '',
            firstName: '',
            phone: '',
            formErrors: {lastName: '', firstName: '', phone: ''},
            lastNameValid: false,
            firstNameValid: false,
            phoneValid: false,
            formValid: false
        }
    }


    handleContactInput = function (e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value},
            () => {
                this.validateField(name, value)
            });
    };


    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let lastNameValid = this.state.lastNameValid;
        let firstNameValid = this.state.firstNameValid;
        let phoneValid = this.state.phoneValid;


        switch (fieldName) {
            case 'lastName':
                lastNameValid = value.match('([^\\s*]+)', 'g');
                fieldValidationErrors.lastName = lastNameValid ? '' : 'Поле Фамилия должно быть заполнено.';
                break;
            case 'firstName':
                firstNameValid = value.match('([^\\s*]+)', 'g');
                fieldValidationErrors.firstName = firstNameValid ? '' : 'Поле Имя должно быть заполнено.';
                break;
            case 'phone':
                phoneValid = value;
                fieldValidationErrors.phone = phoneValid ? '' : 'Поле Телефон должно быть заполнено.';
                let sameContact = Lazy(this.props.contacts).find({phone: phoneValid});
                if (sameContact) {
                    phoneValid = '';
                    fieldValidationErrors.phone = 'Номер телефона не должен дублировать другие номера в телефонной книге.'
                }
                break;
            default:
                break;
        }

        this.setState({
            formErrors: fieldValidationErrors,
            lastNameValid: lastNameValid,
            firstNameValid: firstNameValid,
            phoneValid: phoneValid
        }, this.validateForm);
    }


    validateForm() {
        this.setState({formValid: this.state.lastNameValid && this.state.firstNameValid && this.state.phoneValid});
    }


    static errorClass(error) {
        return (error.length === 0 ? '' : 'has-error');
    }


    render() {
        return (
            <form className='form'>
                <div className={`${InputForm.errorClass(this.state.formErrors.lastName)}`}>
                    <label className='form-label'>
                        <span className='form-field'>Фамилия:</span>
                        <input name='lastName' type='text' className="form-control input-sm form-input"
                               value={this.state.lastName} onChange={(e) => {
                            this.handleContactInput(e)
                        }}/>
                        <span className="error-message"> {this.state.formErrors.lastName}</span>
                    </label>
                </div>
                <div className={`${InputForm.errorClass(this.state.formErrors.firstName)}`}>
                    <label className='form-label'>
                        <span className='form-field'>Имя:</span>
                        <input type='text' name='firstName' className="form-control input-sm form-input"
                               value={this.state.firstName} onChange={(e) => {
                            this.handleContactInput(e)
                        }}/>
                        <span className="error-message"> {this.state.formErrors.firstName}</span>
                    </label>
                </div>
                <div className={`${InputForm.errorClass(this.state.formErrors.phone)}`}>
                    <label className='form-label'>
                        <span className='form-field'>Телефон:</span>
                        <input type='number' name='phone' className="form-control input-sm form-input"
                               value={this.state.phone} onChange={(e) => {
                            this.handleContactInput(e)
                        }}/>
                        <span className="error-message"> {this.state.formErrors.phone}</span>
                    </label>
                </div>
                <Button type='button' bsClass='btn' bsSize="sm" bsStyle="primary" disabled={!this.state.formValid}
                        onClick={() => {
                            this.addContact()
                        }}>Добавить
                </Button>
            </form>
        )
    }

    addContact() {
        this.props.onAddContact(this.state.lastName, this.state.firstName, this.state.phone);

        this.setState({
            firstName: '',
            lastName: '',
            phone: '',
            lastNameValid: false,
            firstNameValid: false,
            phoneValid: false,
            formValid: false
        })
    }
}
