import * as types from "../constants/actionTypes";
import Lazy from 'lazy.js/lazy';

export default function calls(state = {
    items: [],
    isLoading: false,
    errors: {}
}, action) {
    switch (action.type) {
        case types.FETCH_ALL_CALLS:
            return Object.assign({}, state, {
                isLoading: true
            });
            break;
        case types.FETCH_ALL_CALLS_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                items: action.payload
            });
            break;
        case types.FETCH_ALL_CALLS_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                errors: action.errors
            });
            break;
        case types.ADD_CALL:
            return Object.assign({}, state, {
                items: [...state.items, action.payload],
                isLoading: false,
                errors: {}
            });
            break;
        case types.REMOVE_CALL:
            return Object.assign({}, state, {
                items: Lazy(state.items).filter(call => {
                    if (call.id !== action.payload) {
                        return call;
                    }
                }).toArray(),
                isLoading: false,
                errors: {}
            });
            break;
        case types.REMOVE_CALLS_WITH_CONTACT:
            return Object.assign({}, state, {
                items: Lazy(state.items).filter(call => {
                    if (call.contact.id !== action.payload) {
                        return call;
                    }
                }).toArray(),
                isLoading: false,
                errors: {}
            });
            break;
        case types.REMOVE_CALLS_WITH_SEVERAL_CONTACTS:
            return Object.assign({}, state, {
                items: Lazy(state.items).filter(call => {
                    if (!Lazy(action.payload).contains(call.contact.id)) {
                        return call
                    }
                }).toArray(),
                isLoading: false,
                errors: {}
            });
            break;
        default:
            return state;
    }
}