import Lazy from 'lazy.js/lazy'
import Contact from "../components/Contact";
import * as types from "../constants/actionTypes";
import fetch from "isomorphic-fetch";

export const getAllContactsFromServer = () => dispatch => {
    dispatch({type: types.FETCH_ALL_CONTACTS});
    fetch('/phoneBook/rcp/api/v1/contact/getAll')
        .then(response =>
            response.json())
        .then((contacts) => {
            let contactListForClient = [];
            Lazy(contacts).each(contact => {
                let contactForClient = new Contact(contact.id, contact.firstName,
                    contact.lastName, contact.phone, contact.important, false);
                contactListForClient.push(contactForClient);
            });
            dispatch({type: types.FETCH_ALL_CONTACTS_SUCCESS, payload: contactListForClient})
        }).catch(errors => dispatch({type: types.FETCH_ALL_CONTACTS_ERROR, errors: errors}))
};


export const findContact = (event) => dispatch => {
    let searchContact = event.target.value.toLowerCase();
    dispatch({type: types.FIND_CONTACT, payload: searchContact})
};

export const addContact = (lastName, firstName, phone) => dispatch => {
    let init = {
        method: 'POST',
        dataType: 'json',
        headers: {
            "Content-type": "application/json; charset=utf-8"
        },
        body: JSON.stringify({lastName, firstName, phone})
    };

    fetch('/phoneBook/rcp/api/v1/contact/add', init)
        .then((response) => {
            return response.json()
        })
        .then((data) => {
                if (data.contactValidation.valid) {
                    let contactForClient = new Contact(data.contact.id, data.contact.firstName,
                        data.contact.lastName, data.contact.phone, data.contact.important, false);
                    dispatch({type: types.ADD_CONTACT, payload: contactForClient})
                } else {
                    dispatch({type: types.FETCH_ALL_CALLS_ERROR, errors: alert(data.contactValidation.error)})
                }
            }
        );
};


export const removeContact = (id) => dispatch => {
    let init = {
        method: 'POST',
        dataType: 'json',
        headers: {
            "Content-type": "application/json; charset=utf-8"
        },
        body: JSON.stringify(id)
    };

    fetch('/phoneBook/rcp/api/v1/contact/remove', init)
        .then(() => {
            dispatch({type: types.REMOVE_CONTACT, payload: id});
            dispatch({type: types.REMOVE_CALLS_WITH_CONTACT, payload: id});
            dispatch({type: types.REMOVE_CHECKED});

        })
};


export const removeSelectedContacts = (contacts) => {
    return dispatch => {
        let selectedContactsById = Lazy(contacts).filter(function (contact) {
            return contact.checked;
        }).pluck('id').toArray();

        let init = {
            method: 'POST',
            dataType: 'json',
            headers: {
                "Content-type": "application/json; charset=utf-8"
            },
            body: JSON.stringify(selectedContactsById)
        };

        fetch('/phoneBook/rcp/api/v1/contact/removeSelected', init).then(() => {
            dispatch({type: types.REMOVE_SELECTED_CONTACT, payload: selectedContactsById});
            dispatch({type: types.REMOVE_CALLS_WITH_SEVERAL_CONTACTS, payload: selectedContactsById});
            dispatch({type: types.CHECKED_ALL, payload: false});
            dispatch({type: types.CHECK_SIZE_CHECKED, payload: selectedContactsById.length});
        });
    };
};


export const markSelectedContacts = (contacts) => dispatch => {
    let selectedContactsById = Lazy(contacts).filter(function (contact) {
        return contact.checked;
    }).pluck('id').toArray();

    let init = {
        method: 'POST',
        dataType: 'json',
        headers: {
            "Content-type": "application/json; charset=utf-8"
        },
        body: JSON.stringify(selectedContactsById)
    };

    fetch('/phoneBook/rcp/api/v1/contact/markSelected', init).then(() => {
        dispatch({type: types.MARK_SELECTED_CONTACT, payload: selectedContactsById});
        dispatch({type: types.CHECKED_ALL, payload: false});
        dispatch({type: types.SELECT_ALL_CHECKBOX, payload: false});
    });
};






