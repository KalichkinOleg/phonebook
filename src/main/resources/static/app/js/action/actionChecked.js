import * as types from "../constants/actionTypes";

export const addCheckbox = (checked) => dispatch => {
    dispatch({type: types.ADD_CHECK, payload: checked})
};


export const changeChecked = (index, event) => dispatch => {
    let checked = event.target.checked;

    dispatch({type: types.CHANGE_CHECKED, payload: {index: index, checked: checked}});
    dispatch({type: types.CHECKED_CONTACT, payload: {index: index, checked: checked}});
};



export const selectAllCheckbox = (event) => dispatch => {
    dispatch({type: types.SELECT_ALL_CHECKBOX, payload: event.target.checked});
    dispatch({type: types.CHECKED_ALL, payload: event.target.checked})
};