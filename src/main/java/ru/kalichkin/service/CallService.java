package ru.kalichkin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kalichkin.dao.callDao.CallDao;
import ru.kalichkin.model.Call;

import java.util.List;

@Service
public class CallService {

    @Autowired
    private CallDao callDao;

    public Call addCall(Call call) {
        return callDao.save(call);
    }

    public void removeCall(int callId) {
        callDao.removeCall(callId);
    }

    public List<Call> getAllCalls() {
        return callDao.getAllCalls();
    }

}