import * as types from "../constants/actionTypes";
import Call from "../components/Call";
import Lazy from 'lazy.js/lazy';
import fetch from "isomorphic-fetch";

export const getAllCallsFromServer = () => dispatch => {
    dispatch({type: types.FETCH_ALL_CALLS});
    fetch('/phoneBook/rcp/api/v1/call/getAll')
        .then(response =>
            response.json())
        .then((calls) => {
            let callListForClient = [];
            Lazy(calls).each(contact => {
                let callForClient = new Call(calls.id, calls.time, calls.contact);
                callListForClient.push(callForClient);
            });
            dispatch({type: types.FETCH_ALL_CALL_SUCCESS, payload: callListForClient})
        }).catch(errors => dispatch({type: types.FETCH_ALL_CALLS_ERROR, errors: errors}))
};


export const addCall = (contact) => dispatch => {
    let time = new Date();

    let init = {
        method: 'POST',
        dataType: 'json',
        headers: {
            "Content-type": "application/json; charset=utf-8"
        },
        body: JSON.stringify({time, contact})
    };


    fetch('/phoneBook/rcp/api/v1/call/add', init)
        .then((response) => {
            return response.json()
        })
        .then((data) => {
                let callForClient = new Call(data.id, data.time, contact);
                dispatch({type: types.ADD_CALL, payload: callForClient})
            }
        );
};


export const removeCall = (id) => dispatch => {
    let init = {
        method: 'POST',
        dataType: 'json',
        headers: {
            "Content-type": "application/json; charset=utf-8"
        },
        body: JSON.stringify(id)
    };

    fetch('/phoneBook/rcp/api/v1/call/remove', init)
        .then(() => {
            dispatch({type: types.REMOVE_CALL, payload: id});
        })
};