package ru.kalichkin.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.kalichkin.model.Call;
import ru.kalichkin.service.CallService;

import java.util.List;

@Controller
@RequestMapping("/phoneBook/rcp/api/v1/call")
public class CallController {
    private static final Logger logger = LoggerFactory.getLogger(CallController.class);

    @Autowired
    private CallService callService;

    @RequestMapping(value = "getAll", method = RequestMethod.GET)
    @ResponseBody
    public List<Call> getAll() {
        return callService.getAllCalls();
    }


    @RequestMapping(value = "add", method = RequestMethod.POST)
    @ResponseBody
    public Call addCall(@RequestBody Call call) {
        logger.info("Added a call: " + call.getContact());
        return callService.addCall(call);
    }


    @RequestMapping(value = "remove", method = RequestMethod.POST)
    @ResponseBody
    public void removeCall(@RequestBody int callId) {
        logger.info("Removed a call with id: " + callId);
        callService.removeCall(callId);
    }
}
