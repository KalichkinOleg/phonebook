package ru.kalichkin.dao.callDao;

import ru.kalichkin.model.Call;

import java.util.List;

public interface CallDao {

    List<Call> getAllCalls();

    Call save(Call call);

    void removeCall(int callId);
}
