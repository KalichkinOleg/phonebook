import Lazy from 'lazy.js/lazy';
import * as types from "../constants/actionTypes";

export default function contacts(state = {
    items: [],
    isLoading: false,
    errors: {}
}, action) {
    switch (action.type) {
        case types.FETCH_ALL_CONTACTS:
            return Object.assign({}, state, {
                isLoading: true
            });
            break;
        case types.FETCH_ALL_CONTACTS_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                items: action.payload
            });
            break;
        case types.FETCH_ALL_CONTACTS_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                errors: action.errors
            });
            break;
        case types.ADD_CONTACT:
            return Object.assign({}, state, {
                items: [...state.items, action.payload],
                isLoading: false,
                errors: {}
            });
            break;
        case types.REMOVE_CONTACT:
            return Object.assign({}, state, {
                items: Lazy(state.items).filter(contact => {
                    if (!contact.important) {
                        if (contact.id !== action.payload) {
                            return contact;
                        }
                    }
                }).toArray(),
                isLoading: false,
                errors: {}
            });
            break;
        case types.CHECKED_CONTACT:
            return Object.assign({}, state, {
                items: Lazy(state.items).map((contact, i) => {
                    if (i === action.payload.index) {
                        contact.checked = action.payload.checked;
                    }
                    return contact;
                }).toArray(),
                isLoading: false,
                errors: {}
            });
            break;
        case types.CHECKED_ALL:
            return Object.assign({}, state, {
                items: Lazy(state.items).map(contact => {
                    contact.checked = action.payload;
                    return contact;
                }).toArray(),
                isLoading: false,
                errors: {}
            });
        case types.REMOVE_SELECTED_CONTACT:
            return Object.assign({}, state, {
                items: Lazy(state.items).filter(contact => {
                    if (!Lazy(action.payload).contains(contact.id) || contact.important === true) {
                        return contact;
                    }
                }).toArray(),
                isLoading: false,
                errors: {}
            });
        case types.MARK_SELECTED_CONTACT:
            return Object.assign({}, state, {
                items: Lazy(state.items).map(contact => {
                    if (Lazy(action.payload).contains(contact.id)) {
                        contact.important = !contact.important;
                    }
                    return contact;
                }).toArray(),
                isLoading: false,
                errors: {}
            });
            break;
        default:
            return state;
    }
}
