import React, {Component} from 'react';
import Checkbox from "./Checkbox";
import Button from "react-bootstrap/es/Button";

export default class TableRow extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.onAddCheckbox();
    }

    showNotification(contact) {
        this.props.notify.info("Звонок совершен!", new Date().getHours() + ':' + new Date().getMinutes()
            + '  '  + contact.firstName + ' ' + contact.lastName, 3000);
    }

    render() {
        return (
            <tr className={this.props.contact.important ? 'danger' : ''}>
                <td className='text-center'>
                    <label className='check-box-table-cell'>
                        <Checkbox checked={this.props.checked}
                                  onChange={(event) => {
                                      this.props.onChangeChecked(event)
                                  }}/>
                    </label>
                </td>
                <td className='text-center'>{this.props.number}</td>
                <td className='text-center'>{this.props.contact.lastName}</td>
                <td className='text-center'>{this.props.contact.firstName}</td>
                <td className='text-center'>{this.props.contact.phone}</td>
                <td className='text-center'>
                    <Button bsClass='btn' bsStyle="info" bsSize="xs" onClick={() => {
                        this.props.onAddCall(this.props.contact);
                    }} type='button'>Позвонить
                    </Button>
                </td>
                <td className='text-center'>
                    <Button bsClass={this.props.contact.important ? 'hidden' : 'btn'} bsStyle="danger" bsSize="xs"
                            onClick={() => {
                                this.props.onRemoveContact(this.props.contact.id)
                            }} type='button'>Удалить
                    </Button>
                </td>
            </tr>
        );
    }
}