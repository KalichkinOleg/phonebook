package ru.kalichkin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.kalichkin.converter.ContactDtoConverter;
import ru.kalichkin.dto.ContactDto;
import ru.kalichkin.model.Contact;
import ru.kalichkin.service.ContactService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.kalichkin.service.ExcelView;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


@Controller
@RequestMapping("/phoneBook/rcp/api/v1/contact")
public class ContactController {
    private static final Logger logger = LoggerFactory.getLogger(ContactController.class);

    @Autowired
    private ContactService contactService;

    @Autowired
    private ContactDtoConverter contactDtoConverter;

    @RequestMapping(value = "getAll", method = RequestMethod.GET)
    @ResponseBody
    public List<Contact> getAllContacts() {
        logger.info("called method getAllContacts");
        return contactService.getAllContacts();
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    @ResponseBody
    public ContactDto addContact(@RequestBody Contact contact) {
        logger.info("Added a contact: " + contact);
        return contactDtoConverter.map(contactService.addContact(contact), contact);
    }

    @RequestMapping(value = "remove", method = RequestMethod.POST)
    @ResponseBody
    public void removeContact(@RequestBody int contactId) {
        contactService.removeContact(contactId);
        logger.info("Removed a contact: " + contactId);
    }

    @RequestMapping(value = "removeSelected", method = RequestMethod.POST)
    @ResponseBody
    public void removeSelectedContact(@RequestBody int[] contactsId) {
        contactService.removeSelectedContact(contactsId);
        logger.info("Removed several contacts with id: " + Arrays.toString(contactsId));
    }

    @RequestMapping(value = "markSelected", method = RequestMethod.POST)
    @ResponseBody
    public void markSelectedContact(@RequestBody int[] contactsId) {
        contactService.markSelectedContact(contactsId);
        logger.info("Marked several contacts with id: " + Arrays.toString(contactsId));
    }

    @GetMapping("download")
    public void download(HttpServletResponse response) throws IOException {
        ExcelView.createFile(contactService, response);
        logger.info("Created Excel file");
    }
}

