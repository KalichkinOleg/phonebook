import React, {Component} from 'react';
import Lazy from 'lazy.js/lazy';

export default class CallTable extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <table className={this.props.className}>
                <thead>
                <tr className='text-center'>
                    <th className='text-center'>№</th>
                    <th className='text-center'>Дата и время</th>
                    <th className='text-center'>Контакст</th>
                    <th className='text-center'>Удалить</th>
                </tr>
                </thead>
                <tbody>
                {
                    Lazy(this.props.calls).map((call, i) => {
                        let timeToServer = new Date(call.time);
                        let time = timeToServer.getDate() + '.' + timeToServer.getMonth() + '.' + timeToServer.getFullYear()
                            + ' ' + timeToServer.getHours() + ':';
                        if (timeToServer.getMinutes() < 10) {
                            time = time + '0' + timeToServer.getMinutes();
                        } else {
                            time = time + timeToServer.getMinutes();
                        }
                        let contact = call.contact.firstName + " " + call.contact.lastName + " " + call.contact.phone;
                        return <tr key={call.id}>
                            <td className='text-center'>{i + 1}</td>
                            <td className='text-center'>{time}</td>
                            <td className='text-center'>{contact}</td>
                            <td className='text-center'>
                                <button className='btn btn-xs btn-danger' type='button' onClick={() => {
                                    this.props.onRemoveCall(call.id)
                                }}>Удалить
                                </button>
                            </td>
                        </tr>
                    }).toArray()
                }
                </tbody>
            </table>
        )
    }
}

